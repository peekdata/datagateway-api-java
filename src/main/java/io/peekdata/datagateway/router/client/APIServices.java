package io.peekdata.datagateway.router.client;

import io.peekdata.datagateway.router.client.model.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.io.FileUtils;

/**
* Class contains Example of main Data API Gateway methods to be used for your application
* https://demo.peekdata.io:8443/ is public and available for testing
* @author  Dionizas Antipenkovas
* @version 1.8.0
*/ 
public class APIServices {
    
    private HttpHost host; 
    private HttpClient httpClient;
    
    public APIServices(String url, int port, String scheme) {
        
        this.host = new HttpHost(url, port, scheme);
        this.httpClient = HttpClientBuilder.create().build();
    }
    // Methods for latest v1.8.0 Data API
    private final String CONST_API_METHOD_HEALTHCHECK = "/datagateway/rest/v1/healthcheck";
    private final String CONST_API_METHOD_GETQUERY = "/datagateway/rest/v1/query/get";
    private final String CONST_API_METHOD_GETDATA = "/datagateway/rest/v1/data/get";
    private final String CONST_API_METHOD_GETDATA_OPTIMIZED = "/datagateway/rest/v1/data/getoptimized";
    private final String CONST_API_METHOD_GETCSV = "/datagateway/rest/v1/file/get";
    
    /**
    * Method to check service status. If configuration of engine is it ok
    *
    * @author  Dionizas Antipenkovas
    * @version 1.8.0
    * @return True if operation was successful
    * @since   2019-12-07 
    */ 
    public boolean healthCheck() {
    
        try {    
            // specify the get request
            HttpGet getRequest = new HttpGet(CONST_API_METHOD_HEALTHCHECK);
            // call API
            HttpResponse response = httpClient.execute(this.host, getRequest);
            // Get Result
            StatusLine status = response.getStatusLine();
            // check
            if (status.getStatusCode() != 200) {
                // Error
                return false;
            }
        } 
        catch (Exception e) {
          e.printStackTrace();
          return false;
        } 
        return true;
    }
    
    /**
    * Returns Query(SELECT statement) based on request
    *
    * @author  Dionizas Antipenkovas
    * @version 1.8
    * @param request - parameters to API, DataSource is mandatory in this case
    * @return Query statement
    * @since   2019-12-07 
    */ 
    public String getQuery(DataRequestDto request) {
     
        HttpPost postRequest = new HttpPost(CONST_API_METHOD_GETQUERY);
        try {
            String paramsJson = new ObjectMapper().writeValueAsString(request);

            StringEntity params = new StringEntity(paramsJson);
            postRequest.addHeader("Content-type", "application/json");
            postRequest.setEntity(params);

            HttpResponse response = this.httpClient.execute(this.host, postRequest);

            if (response == null) {
                return null;
            }
            // Get Result
            StatusLine status = response.getStatusLine();
            // check
            if (status.getStatusCode() != 200) {
                // Error
                return null;
            }

            try {           
                return(readContent(response));
            } 
            catch (Exception e) {
                e.printStackTrace();
            } 
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
   
    /**
    * Used to get data from API
    *
    * @author  Dionizas Antipenkovas
    * @version 1.6
    * @param request
    * @return DTO containing data
    * @since   2019-12-07 
    */ 
    public DataResponseDto getData(DataRequestDto request) {
     
        HttpPost postRequest = new HttpPost(CONST_API_METHOD_GETDATA);
        try {
            String paramsJson = new ObjectMapper().writeValueAsString(request);

            StringEntity params = new StringEntity(paramsJson);
            postRequest.addHeader("Content-type", "application/json");
            postRequest.setEntity(params);

            HttpResponse response = this.httpClient.execute(this.host, postRequest);

            if (response == null) {
                return null;
            }
            // Get Result
            StatusLine status = response.getStatusLine();
            // check
            if (status.getStatusCode() != 200) {
                // Error
                return null;
            }

            // read data and parse to result
            try {                                                   
                DataResponseDto data = new ObjectMapper().readValue(readContent(response), DataResponseDto.class);
                
                return(data);
            } 
            catch (Exception e) {
                e.printStackTrace();
            } 
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
   
    /**
    * Used to get data from API
    *
    * @author  Dionizas Antipenkovas
    * @version 1.8.0
    * @param request
    * @return DTO containing data in optimized JSON format, meaning names of columns are cut to minimize data size
    * @since   2019-12-07 
    */ 
    public DataOptimizedResponseDto getDataOptimized(DataRequestDto request) {
     
        HttpPost postRequest = new HttpPost(CONST_API_METHOD_GETDATA_OPTIMIZED);
        try {
            String paramsJson = new ObjectMapper().writeValueAsString(request);

            StringEntity params = new StringEntity(paramsJson);
            postRequest.addHeader("Content-type", "application/json");
            postRequest.setEntity(params);

            HttpResponse response = this.httpClient.execute(this.host, postRequest);

            if (response == null) {
                return null;
            }
            // Get Result
            StatusLine status = response.getStatusLine();
            // check
            if (status.getStatusCode() != 200) {
                // Error
                return null;
            }

            // read data and parse to result
            try {           
                DataOptimizedResponseDto data = new ObjectMapper().readValue(readContent(response), DataOptimizedResponseDto.class);
                
                return(data);
            } 
            catch (Exception e) {
                e.printStackTrace();
            } 
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
             
    /**
    * Saves data to CSV file
    *
    * @author  Dionizas Antipenkovas
    * @version 1.8.0
    * @param request - parameters to API
    * @param fileName - fileName to save data 
    * @return True if operation was successful
    * @since   2019-12-07 
    */ 
    public boolean getCSV(DataRequestDto request, String fileName) {
     
        HttpPost postRequest = new HttpPost(CONST_API_METHOD_GETCSV);
        try {
            String paramsJson = new ObjectMapper().writeValueAsString(request);

            StringEntity params = new StringEntity(paramsJson);

            postRequest.addHeader("Content-type", "application/json");
            postRequest.setEntity(params);

            HttpResponse response = this.httpClient.execute(this.host, postRequest);

            if (response == null) {
                return false;
            }
            // Get Result
            StatusLine status = response.getStatusLine();
            // check
            if (status.getStatusCode() != 200) {
                // Error
                return false;
            }

            // read data and parse to result
            InputStream inputStream = response.getEntity().getContent();
            
            try {           
                File file = new File(fileName);
                FileUtils.copyInputStreamToFile(inputStream, file);
            
                return(true);
            } 
            catch (Exception e) {
                e.printStackTrace();
            } 
            finally {
                try { 
                    inputStream.close(); 
                } 
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    
    /**
    * Reads content to get our response in string
    *
    * @author  Dionizas Antipenkovas
    * @version 1.8.0
    * @param response - HTTP response
    * @return string to be converted to class
    * @since   2019-12-07 
    */ 
    private String readContent(HttpResponse response) throws Exception{
        // read data and parse to result
            InputStream inputStream = response.getEntity().getContent();            
            try {           
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String read = null;
                StringBuilder sb = new StringBuilder();

                while((read = bufferedReader.readLine()) != null) {
                    sb.append(read);
                }
                
                return(sb.toString());
            }
            catch(IOException e) {
                throw new Exception (String.format("Failed reading response: %s", e.getMessage()));
            } 
            finally {
                try { 
                    inputStream.close(); 
                } 
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }
    
}
