package io.peekdata.datagateway.router.client;

import io.peekdata.datagateway.router.client.model.*;
import java.util.ArrayList;

/**
 *
 * @author d.antipenkovas
 */
public class RequestServices {
    
    public static DataRequestDto getTwoDimensionsTwoMetricsFilterAndSorting() {

        DataRequestDto request = new DataRequestDto();

        request.scopeName = "Mortgage-Lending";

        request.dimensions = new String[2];
        request.dimensions[0] = "propertyCityID";
        request.dimensions[1] = "currency";

        request.metrics = new String[2];
        request.metrics[0] = "loanamount";
        request.metrics[1] = "totalincome";

        request.filters = new FilterDto();
        request.filters.singleKeys = new ArrayList();

        SingleKeyFilterDto currency = new SingleKeyFilterDto();

        currency.key = "currency";
        currency.operation = FilterOperation.EQUALS;
        currency.values = new String[]{"EUR"};

        request.filters.singleKeys.add(currency);

        request.sortings = new SortDto();
        request.sortings.dimensions = new SortKeyDto[1];
        request.sortings.dimensions[0] = new SortKeyDto("currency", SortDirection.ASC);

        return request;
    }
    
    public static DataRequestDto getTwoMetricsAndTwoFilterFromSpecifiedDataModel() {

        DataRequestDto request = new DataRequestDto();

        request.scopeName = "Mortgage-Lending";
        request.dataModel = "Servicing-PostgreSQL";

        request.metrics = new String[2];
        request.metrics[0] = "loanamount";
        request.metrics[1] = "waintrate";

        request.filters = new FilterDto();
        request.filters.dateRanges = new ArrayList();
        request.filters.dateRanges.add(new DateRangeFilterDto("closingdate", "2017-01-01","2017-12-31"));

        request.filters.singleKeys = new ArrayList();
        request.filters.singleKeys.add(new SingleKeyFilterDto("officerid", FilterOperation.EQUALS, new String[] { "1", "2", "3" }));

        return request;
    }
   
    
}
