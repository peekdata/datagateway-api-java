package io.peekdata.datagateway.router.client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;

/**
 *
 * @author begizz, peekdata.io
 */
@JsonSerialize(using = RowDataSerializer.class)
@JsonDeserialize(using = RowDataDeSerializer.class)
public class RowDataDto {
    public HashMap<String,Object> vals = new HashMap();
    public RowDataDto(){
    }
}
