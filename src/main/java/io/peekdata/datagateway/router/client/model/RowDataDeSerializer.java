package io.peekdata.datagateway.router.client.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author begizz, peekdata.io
 */
public class RowDataDeSerializer extends StdDeserializer<RowDataDto> {

    public RowDataDeSerializer(){
        this(null);
    }
    public RowDataDeSerializer(Class<RowDataDto> t){
        super(t);
    }
    
    @Override
    public RowDataDto deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {     
        ObjectMapper mapper = new ObjectMapper();
        HashMap<String, Object> map = new HashMap<>();
        map = mapper.readValue(jp, new TypeReference<Map<String, Object>>(){});
        RowDataDto item = new RowDataDto();
        item.vals = map;
        return item;
    }
    
}
