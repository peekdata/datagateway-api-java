package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class SingleKeyFilterDto {
    public String key;
    public FilterOperation operation;
    public String[] values;
    
    public SingleKeyFilterDto() {
    }
    
    public SingleKeyFilterDto(String key, FilterOperation operation, String[] values) {
        this.key = key;
        this.operation = operation;
        this.values = values;
    }
}
