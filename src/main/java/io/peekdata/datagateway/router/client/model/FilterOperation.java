package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public enum FilterOperation {
    EQUALS,
    NOT_EQUALS,
    STARTS_WITH,
    NOT_STARTS_WITH,
    ALL_IS_LESS,
    ALL_IS_MORE,
    AT_LEAST_ONE_IS_LESS,
    AT_LEAST_ONE_IS_MORE;   
}
