package io.peekdata.datagateway.router.client.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class DataRequestOptionDto {
    public Map<String, String> rows = new HashMap<>();
    public Map<String, String> arguments = new HashMap<>();
}
