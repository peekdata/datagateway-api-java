package io.peekdata.datagateway.router.client.model;

import java.util.List;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class FilterDto {    
    public List<DateRangeFilterDto> dateRanges;
    
    public List<SingleKeyFilterDto> singleKeys;
    
    public List<SingleValueFilterDto> singleValues;

    public FilterDto () {
        
    }
}
