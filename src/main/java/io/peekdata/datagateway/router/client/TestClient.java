package io.peekdata.datagateway.router.client;

import io.peekdata.datagateway.router.client.model.*;
import java.util.Arrays;
import java.util.Collections;
/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class TestClient {
    public final static void main(String[] args) {
        try {
            APIServices apiClient = new APIServices("demo.peekdata.io", 8443, "https");
            // calling services
            System.out.println("Peekdata Data API Gateway examples");
            System.out.println("HealthCheck, is it ok?");
            System.out.println(apiClient.healthCheck());
            
            System.out.println("Calling getQuery:");
            System.out.println(apiClient.getQuery(RequestServices.getTwoDimensionsTwoMetricsFilterAndSorting()));
            
            System.out.println("Calling getDataOptimized:");
            
            DataOptimizedResponseDto odata = apiClient.getDataOptimized(RequestServices.getTwoDimensionsTwoMetricsFilterAndSorting());
            if (odata!=null) {
                System.out.print("Headers: ");
                odata.columnHeaders.forEach((o)-> {
                    System.out.print(o.name+";");
                });
                System.out.println();                
                odata.rows.forEach((o)->System.out.println(Arrays.asList(o)));
            }
                        
            System.out.println("Calling getData:");
            DataResponseDto data = apiClient.getData(RequestServices.getTwoDimensionsTwoMetricsFilterAndSorting());
            // show results
            if (data!=null) {
                System.out.print("Headers: ");
                data.columnHeaders.forEach((o)-> {
                    System.out.print(o.name+";");
                });
                System.out.println();
                
                if (data.rows!=null) {
                    // print out Holumn headers
                    data.rows.forEach((o) -> {
                        System.out.println(Collections.singletonList(o.vals));                   
                    });
                }
                else {
                    System.out.println("No data is received");
                }
            }
            
            System.out.println("Calling savig to CSV file...");
            apiClient.getCSV(RequestServices.getTwoDimensionsTwoMetricsFilterAndSorting(),"C:\\temp\\java_client_01.csv");
            System.out.println("Finished");
            System.out.println();
        }
        catch(Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
