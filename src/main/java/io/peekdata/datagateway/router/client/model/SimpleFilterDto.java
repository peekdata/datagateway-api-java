package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class SimpleFilterDto {
    public String key;
    public boolean isMetric = false;
    public String[] values;
    public FilterDataType type;
}
