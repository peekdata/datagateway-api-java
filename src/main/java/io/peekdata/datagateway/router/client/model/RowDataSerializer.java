
package io.peekdata.datagateway.router.client.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author begizz, peekdata.io
 */
public class RowDataSerializer extends StdSerializer<RowDataDto>{
    public RowDataSerializer(){
        this(null);
    }
    public RowDataSerializer(Class<RowDataDto> t){
        super(t);
    }
    
    @Override
    public void serialize(RowDataDto value, 
            JsonGenerator jgen, SerializerProvider provider) 
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        for(Map.Entry<String,Object> entry : value.vals.entrySet()){
            jgen.writeObjectField(entry.getKey(), entry.getValue());
        }
        jgen.writeEndObject();
    }
}
