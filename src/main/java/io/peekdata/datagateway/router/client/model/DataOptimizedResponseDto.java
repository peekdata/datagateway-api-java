package io.peekdata.datagateway.router.client.model;

import java.util.List;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class DataOptimizedResponseDto {
    
    public String requestID;
    public List<ColumnHeader> columnHeaders;
    public List<Object[]> rows;
    public Integer totalRows = 0;

    public DataOptimizedResponseDto() {
    }
}

