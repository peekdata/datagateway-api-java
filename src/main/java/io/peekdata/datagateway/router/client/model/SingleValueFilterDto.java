package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class SingleValueFilterDto {
    public String[] keys;
    public FilterOperation operation;
    public String value;
    
    public SingleValueFilterDto() {
    }
    
    public SingleValueFilterDto(String[] keys, String value) {
        this.keys = keys;
        this.operation = FilterOperation.EQUALS;
        this.value = value;
    }
    
    public SingleValueFilterDto(String[] keys, FilterOperation operation, String value) {
        this.keys = keys;
        this.operation = operation;
        this.value = value;
    }
}
