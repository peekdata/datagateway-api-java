package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class SortKeyDto {
    public String key;
    public SortDirection direction = SortDirection.ASC;

    public SortKeyDto() {
    }

    public SortKeyDto(String key) {
        this(key, SortDirection.ASC);
    }

    public SortKeyDto(String key, SortDirection direction) {
        this.key = key;
        this.direction = direction;
    }
}
