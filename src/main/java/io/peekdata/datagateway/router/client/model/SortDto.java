package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class SortDto {
    public SortKeyDto[] dimensions;
    public SortKeyDto[] metrics;
}
