package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public enum FilterDataType {
    AUTO,
    NUMBER, 
    DATE,
    STRING,
    CHAR   
}
