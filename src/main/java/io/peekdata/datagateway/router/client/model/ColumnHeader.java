package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class ColumnHeader {
    public String title;
    public String name;
    public String dataType;
    public String format;
    public String alias;
    public String columnType;
    
    public ColumnHeader() {
    }   
}
