package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class DataRequestDto {
    
    private String requestID;    
    public String consumerInfo;
    public String scopeName;
    public String dataModel;
    public String[] dimensions;
    public String[] metrics;
    public FilterDto filters;
    public SortDto sortings;
    public DataRequestOptionDto options;        
}


