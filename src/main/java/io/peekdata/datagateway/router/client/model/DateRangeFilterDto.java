package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public class DateRangeFilterDto {
    public String key;
    public String from;
    public String to;
    
    public DateRangeFilterDto() {
    }

    public DateRangeFilterDto(String from, String to) {
        this.from = from;
        this.to = to;
    }
    
    public DateRangeFilterDto(String key, String from, String to) {
        this.key = key;
        this.from = from;
        this.to = to;
    }
}
