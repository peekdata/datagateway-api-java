package io.peekdata.datagateway.router.client.model;

/**
 *
 * @author d.antipenkovas, peekdata.io
 */
public enum SortDirection {
    ASC("ASC"), DESC("DESC");

    private final String name;

    private SortDirection(final String value) {
        this.name = value;
    }
}
